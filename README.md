# README #

### Introduction ###

Haunted Night is a unabashedly cliched horror/detective game with a generous amount of social engineering and a curious mathematical bent.

A respected humanitarian in a small town, heir to an influential family, decided to hold an open event in the ancient manor. Locals and celebrities were both welcome, and the income will be donated to charity. Despite rumors of ancestral ghosts haunting the manor, of course.

After all the fun and feasting, the host was found dead at night, and the ancient gates somehow won't open. Fearing an unknown murderer, people gathered and decided to stay together before the police arrive, but they are not safe as the killers must be lurking somewhere. Is it supernatural curse or old-fashioned crime? Try to stay alive throughout the Haunted Night and find out for yourself!

### What is this repository for? ###

This is a prototype of Haunted Night, a mystery/horror game in Ensemble Engine.

### How do I get set up? ###

Run server from project folder:
python -m SimpleHTTPServer 8000
or if using python 3: python -m http.server 8000

Then visit:
http://localhost:8000/Haunted.html

### How to play ###

Every turn you will see buttons listing all possible actions your player character can do. After you press a button to take an action, every other NPC will also take an action. You can also see some descriptive text of each NPC next to his or her action buttons. Use common sense when picking actions, and beware that you can get attacked and killed in this game.

The basic way to win is to stay alive until the end, when the sun rises and people can leave the manor. But certainly you'd want to bring the real killer to justice, and that's not as easy!

### Hints ###

There are many possible social states in this game, and the status text next to each character only describes the most salient. Knowing these can be very helpful in winning the game. You can also try to use the graph, a mysterious visualization of certain character relationships that give you extra clues.

This game is not just social manipulation; finding and using items is also important. Some items let you use magic to protect yourself and fight evil spirits, but do not count on them too much.

Balance the living and the dead. You may occasionally have to sacrifice somebody, but every new victim is fresh blood for the host of ghosts.

